import gulp from 'gulp'
import * as sass from 'sass'
import gulpsass from 'gulp-sass'
import imagemin from 'gulp-imagemin'
import htmlmin from 'gulp-htmlmin'
import cssScss from 'gulp-css-scss'
import browserSync from 'browser-sync'

const sassPlugin = gulpsass(sass)
const browser = browserSync.create()

export function html() {
  return gulp.src('src/index.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('build'))
}

export function css() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sassPlugin())
    .pipe(gulp.dest('build/css'))
}

export function img() {
  return gulp.src('src/img/**')
    .pipe(imagemin())
    .pipe(gulp.dest('build/img'))
}

export function convert() {
  return gulp.src('./src/css/**/*.css')
    .pipe(cssScss())
    .pipe(gulp.dest('scss'))
}

function reload(cb) {
  browser.reload()
  cb()
}

export function watch() {
  browser.init({
    server: {
      baseDir: 'build'
    }
  })

  return gulp.watch('src/**/*.*', gulp.series(gulp.parallel(html, css), reload))
}

export default gulp.parallel(html, css, img)